import 'package:flutter/material.dart';
import 'package:modulo_calificar/src/model/list_model.dart';

class ListItem extends StatelessWidget {
  final ListModel list;
  ListItem({this.list});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(list.fecha),
      title: Text(list.servicio),
      subtitle: Text(list.alberquero),
      trailing: Text(list.estado)
    );
  }
}