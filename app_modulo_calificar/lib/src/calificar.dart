import 'package:flutter/material.dart';
import 'package:modulo_calificar/src/model/list_model.dart';
import 'package:modulo_calificar/src/screens/list_item.dart';

class Calificar extends StatelessWidget {
  
  buildList(){
    return <ListModel>[
      ListModel(fecha: "15/05/20", servicio: "Regularión de PH", alberquero: "Carlos Rodriguez", estado: "Pendiente"),
      ListModel(fecha: "23/02/20", servicio: "Depuración de agua", alberquero: "Juan López", estado: "Cancelado"),
      ListModel(fecha: "01/07/20", servicio: "Reparación de bomba", alberquero: "José Vázquez", estado: "Pendiente"),
      ListModel(fecha: "24/08/20", servicio: "Sistema de iluminación", alberquero: "Daniel Martínez", estado: "Finalizado")
    ];
  }

  List<ListItem> buildServicesList(){
    return buildList()
      .map<ListItem>((list) => ListItem(list: list))
      .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: Center(
            child: Text("Servicios")
          )
        )
      ),
      body: ListView(
        children: buildServicesList()
      )
    );
  }
}